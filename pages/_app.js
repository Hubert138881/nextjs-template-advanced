import App from 'next/app'
import Head from 'next/head'

import React from 'react'
import "../styles/styleReset.css"
import Lang from "../components/collectors/LangCollector"
import Cookies from "../components/collectors/CookieOperationsCollector"

/*
* W tym pliku można wykonywać operacje na danych ktore otrzymalismy z getInitialProps elementow
* Także ustala sie zawartosc tagu <head>
*/

export default class MyApp extends App {
    state = {cookieLang: "en"}

    static async getInitialProps({ Component, ctx }) {
        let pageProps = {}
        if (Component.getInitialProps) pageProps = await Component.getInitialProps(ctx)
        if(!ctx.query.lang) ctx.query.lang = ctx.asPath.split("/")[1]

        return Lang.getLangPack(ctx.query.lang, ctx.req.headers, ctx.req.cookies).then(
            (dataPack) => {
                if(!dataPack.prefLang) {
                    pageProps.cookieLang = dataPack.cookieLang
                    let userData = {}
                    for(let key in ctx.req.user) {
                        userData[key] = ctx.req.user[key]
                    }
                    if(pageProps.statusCode) return({pageProps, error: "RENDERERROR", serverData: dataPack.serverData, langPack: dataPack.langPack})
                    return {pageProps, userData, serverData: dataPack.serverData, langPack: dataPack.langPack, query: ctx.query}
                }
                else {
                    pageProps.cookieLang = dataPack.prefLang
                    let splitUrl = ctx.asPath.split("/")
                    let newPath = `/${dataPack.prefLang}`
                    for(let i = 2; i < splitUrl.length; i++) newPath += ("/" + splitUrl[i])

                    return {pageProps, error: "NEWPATH", newPath}
                }
            },
            (err) => {return err}
        )
    }

    componentDidMount() {
        if(!Cookies.get().lang) console.log(Cookies.set("lang", this.props.pageProps.cookieLang, 30 * 24 * 60 * 60 * 1000))
        if(this.props.newPath) location.href = this.props.newPath
    }

    render() {
        const {Component, pageProps, userData, serverData, langPack, query} = this.props;

        switch(this.props.error) {
            case "E1000":
                return (<>
                    <Head> <title>Error E1000</title> </Head>
                    <body>(E1000) We were not able to render this page, please try once again and refresh it</body>
                </>)
            case "E1000-1":
                return (<>
                    <Head> <title>Error E1000-1</title> </Head>
                    <body>(E1000-1) We were not able to render this page, please try once again and refresh it</body>
                </>)
            case "NEWPATH":
                return (<></>)
            case "RENDERERROR":
                return (<>
                    <Head> <title>{langPack.error} {pageProps.statusCode}</title> </Head>
                    <Component predefined={pageProps} serverData={serverData} langPack={langPack}/>
                </>)
            default:
                return (<>
                    <Head> <title>Sample title</title> </Head>
                    <Component predefined={pageProps} query={query} userData={userData} serverData={serverData} langPack={langPack}/>
                </>)
        }
    }
}
