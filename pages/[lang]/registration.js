import React from "react"
import RegistrationForm from "../../components/RegistrationForm"

const Registration = (props) => {
    return <RegistrationForm {...props}/>
}

export default Registration