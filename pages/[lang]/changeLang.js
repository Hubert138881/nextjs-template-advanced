import React from 'react'
import Lang from "../../components/collectors/LangCollector"

const ChangeLang = (props) => {
    return <>
        <label id={"pl"} onClick = {() => {Lang.setLang("pl", 30)}}>pl</label>
        <br/><br/>
        <label id={"en"} onClick = {() => Lang.setLang("en", 30)}>en</label>
    </>
}

 export default ChangeLang