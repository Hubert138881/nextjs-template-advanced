import React from "react"
import LoginForm from "../../components/LoginForm"
import Session from "../../components/collectors/SessionCollector"

const Login = (props) => {
    if(props.userData["_id"]) return(<i>{props.langPack["welcome-you"]}: {props.userData.email}<button onClick={() => Session.delete().then(
        () => {
            alert(props.langPack["logged-out"])
            location.reload()
        },
        () => {
            alert(props.langPack["E1005"])
}
)}>{props.langPack.logout}</button></i>)
else return (<LoginForm {...props}/>)
}

export default Login