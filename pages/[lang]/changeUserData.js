import React, {useEffect} from "react"
import PanelsToChangeUserData from "../../components/PanelsToChangeUserData"
import A from "../../components/collectors/DatabaseCollector"

const ChangeUserData = (props) => {
    let a = new A(props)

    useEffect(() => {
        if(!props.userData["clientId"]) location.href = "login"
    })

    if(props.userData["clientId"]) return (<PanelsToChangeUserData {...props}/>)
    else return (<></>)
}

export default ChangeUserData