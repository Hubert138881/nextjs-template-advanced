import React from 'react'
import {appLog, errorLog} from "../components/utilities/logs"

const Home = (props) => {
    return <p>{props.langPack.welcome}</p>
}
Home.getInitialProps = async (props) => {
    return {lang: props.query}
}
export default Home