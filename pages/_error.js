import React from 'react'

function Error(props) {
    return (
        <p>
            {(() => {
                switch(props.predefined.statusCode) {
                case 404:
                    return (
                        <div>{props.langPack.E404}</div>
                    )
                    break
            }})()}
        </p>
    )
}

Error.getInitialProps = ({ res, err }) => {
    const statusCode = res ? res.statusCode : err ? err.statusCode : 404
    return { statusCode }
}

export default Error
